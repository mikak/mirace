#ifndef HANDSETFILTER_H
#define HANDSETFILTER_H

#include "Constants.h"
#include "PacketData.h"

class HandsetFilter
{
public:
    virtual bool filterData(HandsetData &) { return true; }
};


class HandsetFilterLinear : public HandsetFilter
{
public:
    HandsetFilterLinear(int maxOut, int minIn = 0, int maxIn = MAX_POWER_RAW);

    virtual bool filterData(HandsetData &hsData);

private:
    int mMaxOut;
    int mMinIn;
    int mMaxIn;
};

#endif // HANDSETFILTER_H
