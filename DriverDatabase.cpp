#include "DriverDatabase.h"

#include <QSettings>

DriverDatabase::DriverDatabase()
{
}

void DriverDatabase::readDrivers(QSettings &settings)
{
    int n = settings.beginReadArray("drivers");

    for (int i = 0; i < n; ++i) {
        Driver d;

        settings.setArrayIndex(i);
        d.setName(settings.value("name").toString());
        d.setMaxPower(settings.value("maxpower").toInt());

        mDrivers << d;
    }
    settings.endArray();
}

void DriverDatabase::writeDrivers(QSettings &settings)
{
    settings.beginWriteArray("drivers");

    for (int i = 0; i < mDrivers.size(); ++i) {
        settings.setArrayIndex(i);
        settings.setValue("name", mDrivers[i].name());
        settings.setValue("maxpower", mDrivers[i].maxPower());
    }
    settings.endArray();
}

int DriverDatabase::rowCount(const QModelIndex &) const
{
    return mDrivers.count();
}

QVariant DriverDatabase::data(const QModelIndex &index, int role) const
{
    int i = index.row();

    switch (role) {
    case NameRole:
        return mDrivers[i].name();
    case MaxPowerRole:
        return mDrivers[i].maxPower();
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> DriverDatabase::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole] = "name";
    roles[MaxPowerRole] = "maxPower";
    return roles;
}
