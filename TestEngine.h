#ifndef TESTENGINE_H
#define TESTENGINE_H

#include <QObject>
#include <QTimer>

class TestEngine : public QObject
{
    Q_OBJECT
public:
    explicit TestEngine(QObject *parent = 0);
    ~TestEngine();

signals:
    void lapTimed(int timerId, int totalMs);


public slots:
    void emitLap();

private:
    QTimer mTimer;

    int mTimeElapsed;
    int mCarId;
};

#endif // TESTENGINE_H
