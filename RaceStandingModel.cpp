#include "RaceStandingModel.h"

#include <QtMath>

RaceStandingModel::RaceStandingModel()
{
    mColors << "green" << "red" << "orange"
            << "white" << "yellow" << "blue";
}

RaceStandingModel::~RaceStandingModel()
{
}

int RaceStandingModel::rowCount(const QModelIndex &) const
{
    return 6;
}

QVariant RaceStandingModel::data(const QModelIndex &index, int role) const
{
    const RaceTimeData &t = mRaceTimer.timeData(index.row() + 1);

    switch (role) {
    case DriverRole:
        return QString("Driver Name ") + QString::number(index.row() + 1);
    case ColorRole:
        return mColors.value(index.row(), "grey");
    case LapsRole:
        return qMax(0, t.lapCount());
    case LastLapTimeRole:
        return t.lastLapTime() < 0 ?
                    "----" :
                    QString("%1").arg(t.lastLapTime(), 4, 10, QChar('0'))
                    .insert(-3, QChar('.'));
    default:
        break;
    }

    return QString("TEST");
}

QHash<int, QByteArray> RaceStandingModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[DriverRole] = "driver";
    roles[CarRole] = "car";
    roles[ColorRole] = "colorCode";
    roles[LastLapTimeRole] = "lastLapTime";
    roles[LapsRole] = "laps";
    return roles;
}

void RaceStandingModel::registerLap(int id, int totalTime)
{
    static QVector<int> v = QVector<int>() << LastLapTimeRole << LapsRole;

    mRaceTimer.registerLap(id, totalTime);
    emit dataChanged(index(id - 1), index(id - 1), v);
}
