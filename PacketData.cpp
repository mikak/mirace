
#include "PacketData.h"

#include <QDebug>

#define HS_BRAKE 0x80
#define HS_LANECHANGE 0x40
#define HS_POWER 0x3F

#define TRS_POWER 0x01

#define TRT_CAR_ID 0x07

#define OPMODE_OK 0xFF
#define OPMODE_BAD_CHKSUM 0x7F

#define LEDS_NONE 0x00
#define LEDS_TIMER_RESET 0xC0
#define LEDS_TIMER_START 0x80
#define LEDS_SET_CAR_ID  0x40

HandsetData::HandsetData(int aId, const StatusPacketRaw *raw)
    : id(aId), connected(false), brake(false), laneChange(false), power(0)
{
    setFromRaw(raw);
}

bool HandsetData::setFromRaw(const StatusPacketRaw *raw)
{
    if (raw && id >= HS_ID_FIRST && id <= HS_ID_LAST) {
        connected = raw->status & (1 << id);

        if (connected) {
            quint8 hsByte = ~raw->handset[id - 1];

            brake = hsByte & HS_BRAKE;
            laneChange = hsByte & HS_LANECHANGE;
            power = hsByte & HS_POWER;
        }
    }

    if (!connected)
        clear();

    return connected;
}

bool HandsetData::setToRaw(ControlPacketRaw &raw) const
{
    if (isValid()) {
        raw.drivePacket[id - 1] =
                ~((brake ? HS_BRAKE : 0) |
                  (laneChange ? HS_LANECHANGE : 0) |
                  (power & HS_POWER));
        return true;
    } else {
        return false;
    }
}

bool HandsetData::isValid() const
{
    return (id >= HS_ID_FIRST && id <= HS_ID_LAST);
}

void HandsetData::clear()
{
    connected = false;
    brake = false;
    laneChange = false;
    power = 0;
}

void HandsetData::print() const
{
    qDebug() << "HS" << id
             << "Connected" << connected
             << "Brake" << brake
             << "LC" << laneChange
             << "Power" << power;
}


TrackData::TrackData(const StatusPacketRaw *raw)
{
    setFromRaw(raw);
}

bool TrackData::setFromRaw(const StatusPacketRaw *raw)
{
    if (raw) {
        trackPowered = raw->status & TRS_POWER;
        auxCurrent = raw->auxCurrent;
        timerId = raw->timerId & TRT_CAR_ID;
        if (raw->timer == 0xFFFFFFFF) {
            timeMs = -1;
        } else {
            // 1 tick = 6.4 us
            timeMs = (static_cast<quint64>(raw->timer) << 6) / 10000;
        }
    } else {
        clear();
    }
    return (raw != 0);
}

void TrackData::clear()
{
    trackPowered = false;
    auxCurrent = 0;
    timerId = 0;
    timeMs = 0;
}

void TrackData::print() const
{
    qDebug() << "Track power" << trackPowered
             << "AUX current" << auxCurrent
             << "Timer id" << timerId
             << "Time [ms]" << timeMs;
}

// control packet
ControlPacket::ControlPacket()
{
    mRawPacket.operationMode = OPMODE_OK;
    mRawPacket.leds = LEDS_TIMER_START; //_RESET
    mRawPacket.checksum = 0x00;
    for (int i = 0; i < HS_COUNT; i++) {
        mHandsets[i].id = i + 1;
    }

    makeRaw();
}

QByteArray ControlPacket::makeRaw()
{
    for (int i = 0; i < HS_COUNT; i++) {
        mHandsets[i].setToRaw(mRawPacket);
    }

    return QByteArray(reinterpret_cast<const char *>(&mRawPacket),
                      sizeof(ControlPacketRaw));
}

bool ControlPacket::setDrivePacket(const HandsetData &hs)
{
    if (hs.isValid()) {
        mHandsets[hs.id - 1] = hs;
        return true;
    } else {
        return false;
    }
}
