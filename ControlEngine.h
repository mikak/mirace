#ifndef CONTROLENGINE_H
#define CONTROLENGINE_H

#include <QObject>

#include "Transport.h"
#include "PacketData.h"


class ControlEngine : public QObject
{
    Q_OBJECT
public:
    ControlEngine();
    ~ControlEngine();

    virtual void processPacket(const StatusPacketRaw *packet);

signals:
    void lapTimed(int timerId, int totalMs);

private:
    Transport mTransport;
    ControlPacket mControlPacket;
};

#endif // CONTROLENGINE_H
