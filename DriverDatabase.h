#ifndef DRIVERDATABASE_H
#define DRIVERDATABASE_H

#include <QAbstractListModel>

#include "Driver.h"

class QSettings;

class DriverDatabase : public QAbstractListModel
{
public:
    enum AttributeRole {
        NameRole = Qt::UserRole + 1,
        MaxPowerRole
    };

    DriverDatabase();

    void readDrivers(QSettings &settings);
    void writeDrivers(QSettings &settings);


signals:

public slots:

private:

    QList<Driver> mDrivers;

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;
};

#endif // DRIVERDATABASE_H
