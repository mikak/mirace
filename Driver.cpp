#include "Driver.h"

Driver::Driver()
    : mMaxPower(0)
{
}

QString Driver::name() const
{
    return mName;
}

void Driver::setName(const QString &value)
{
    mName = value;
}
int Driver::maxPower() const
{
    return mMaxPower;
}

void Driver::setMaxPower(int value)
{
    mMaxPower = value;
}


