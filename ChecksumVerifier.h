#ifndef CHECKSUMVERIFIER_H
#define CHECKSUMVERIFIER_H

#include "PacketData.h"

class ChecksumVerifier
{
public:
    ChecksumVerifier();
    ~ChecksumVerifier();

    bool verifyChecksum(const StatusPacketRaw *raw) const;

    quint8 calculateChecksum(const char *data, int len) const;

private:
    static const quint8 crcTable[256];

};

#endif // CHECKSUMVERIFIER_H
