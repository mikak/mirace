#ifndef DRIVER_H
#define DRIVER_H

#include <QString>

class Driver
{
public:
    Driver();

    QString name() const;
    void setName(const QString &value);

    int maxPower() const;
    void setMaxPower(int value);

private:
    QString mName;

    int mMaxPower;
};

#endif // DRIVER_H
