#ifndef RACETIMER_H
#define RACETIMER_H

#include <QObject>
#include <QMap>

class RaceTimeData {
public:
    int lapCount() const { return mLapTimes.count() - 1; }
    int lastLapTime() const;

    int addLapTime(int totalTime);

private:
    int mId;
    QList<int> mLapTimes;
};

class RaceTimer : public QObject
{
    Q_OBJECT
public:
    explicit RaceTimer(QObject *parent = 0);
    ~RaceTimer();

    const RaceTimeData timeData(int id) const;

signals:

public slots:
    void registerLap(int id, int totalTime);

private:
    int mRaceStartTime;
    QMap<int, RaceTimeData> mRaceData;
};

#endif // RACETIMER_H
