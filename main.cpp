#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "ControlEngine.h"
#include "TestEngine.h"
#include "RaceTimer.h"
#include "RaceStandingModel.h"
#include "Database.h"

#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("MK");
    //QCoreApplication::setOrganizationDomain("mysoft.com");
    QCoreApplication::setApplicationName("MiRace");

    QApplication app(argc, argv);

    ControlEngine e;
    //TestEngine e;

    Database db;
    //db.readDrivers();
    RaceStandingModel rm;

    QObject::connect(&e, SIGNAL(lapTimed(int,int)), &rm, SLOT(registerLap(int,int)));

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("raceStandings", &rm);
    engine.rootContext()->setContextProperty("driverDb", &db.driverDb());
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();

}
