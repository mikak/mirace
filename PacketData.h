#ifndef PACKETDATA_H
#define PACKETDATA_H

#include <QtGlobal>

#define HS_COUNT 6
#define PACKED __attribute__ ((packed));

enum HandsetId {
    HS_ID_GAME = 0,
    HS_ID_1 = 1,
    HS_ID_2,
    HS_ID_3,
    HS_ID_4,
    HS_ID_5,
    HS_ID_6,
    HS_ID_INVALID = 7,
    HS_ID_FIRST = HS_ID_1,
    HS_ID_LAST = HS_ID_6
};

struct StatusPacketRaw {
    quint8 status;
    quint8 handset[HS_COUNT];
    quint8 auxCurrent;
    quint8 timerId;
    quint32 timer;
    quint8 powerbaseButtons;
    quint8 checksum;
} PACKED;

struct ControlPacketRaw {
    quint8 operationMode;
    quint8 drivePacket[HS_COUNT];
    quint8 leds;
    quint8 checksum;
} PACKED;


struct HandsetData {
    HandsetData(int aId = 0, const StatusPacketRaw *raw = 0);

    bool setFromRaw(const StatusPacketRaw *raw = 0);
    bool setToRaw(ControlPacketRaw &raw) const;
    bool isValid() const;

    void clear();
    void print() const;

    int id;
    bool connected;

    bool brake;
    bool laneChange;
    int power;
};

class ControlPacket {
public:
    ControlPacket();

    QByteArray makeRaw();

    void setLeds(quint8 x) { mRawPacket.leds = x; }
    bool setDrivePacket(const HandsetData &hs);

private:
    ControlPacketRaw mRawPacket;
    HandsetData mHandsets[HS_COUNT];
};

struct TrackData {
    TrackData(const StatusPacketRaw *raw = 0);

    bool setFromRaw(const StatusPacketRaw *raw = 0);
    void clear();
    void print() const;

    bool trackPowered;
    int auxCurrent;
    int timerId; // HS_ID 0: game, 1-6: car, 7: invalid
    int timeMs;
};

#endif // PACKETDATA_H
