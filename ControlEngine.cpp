#include "ControlEngine.h"
#include "HandsetFilter.h"

#include <QDebug>

ControlEngine::ControlEngine()
{
    mTransport.setEngine(this);
    mTransport.init();
}

ControlEngine::~ControlEngine()
{
}

void ControlEngine::processPacket(const StatusPacketRaw *packet)
{
    static int count = 0;
    static int l = 0;

    bool print = false;

    if (!packet)
        return;

    count++;

    TrackData td(packet);
    if (td.timerId >= HS_ID_FIRST && td.timerId <= HS_ID_LAST) {
        emit lapTimed(td.timerId, td.timeMs);
    }

    if (count % 50 == 5) {
        l++;
        td.print();

        print = true;
        //mControlPacket.setLeds((l & 1) ? 0x3f : 0);
    }

    HandsetFilterLinear lf(20);

    for (int i = HS_ID_FIRST; i <= HS_ID_LAST; i++) {
        HandsetData hs(i, packet);
        lf.filterData(hs);
        if (print) hs.print();
        if (td.timerId == i) td.print();
        mControlPacket.setDrivePacket(hs);
    }

    if (!mTransport.sendPacket(mControlPacket.makeRaw())) {
        qDebug() << "Send failed";
    }
}
