TEMPLATE = app

QT += qml quick widgets serialport

SOURCES += main.cpp \
    PacketParser.cpp \
    PacketData.cpp \
    ChecksumVerifier.cpp \
    Transport.cpp \
    ControlEngine.cpp \
    HandsetFilter.cpp \
    RaceTimer.cpp \
    RaceStandingModel.cpp \
    TestEngine.cpp \
    Driver.cpp \
    Database.cpp \
    DriverDatabase.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    PacketParser.h \
    PacketData.h \
    ChecksumVerifier.h \
    Transport.h \
    ControlEngine.h \
    HandsetFilter.h \
    RaceTimer.h \
    RaceStandingModel.h \
    TestEngine.h \
    Driver.h \
    Constants.h \
    Database.h \
    DriverDatabase.h
