#ifndef CONSTANTS_H
#define CONSTANTS_H

const int MAX_POWER_RAW = 63;
const int MAX_POWER_NORMALIZED = 1000;

#endif // CONSTANTS_H
