import QtQuick 2.0
import QtQuick.Controls 1.2

Rectangle {
    width: 400
    height: 300


    ListModel {
        id: driverModel

        ListElement {
            name: "A"
            maxPower: 50
        }
        ListElement {
            name: "B"
            maxPower: 20
        }
        ListElement {
            name: "C"
            maxPower: 70
        }
    }


    ListView {
        id: driverView

        anchors.fill: parent
        model: driverDb

        delegate: Item {
            width: parent.width
            height: 30

            Row {
                spacing: 20

                TextField {
                    text: name
                    width: 200
                }
                Slider {
                    id: powerSlider
                    minimumValue: 10
                    maximumValue: 100
                    tickmarksEnabled: true
                    stepSize: 5
                    value: maxPower
                }
                Text {
                    text: powerSlider.value
                }
            }
        }
    }

}

