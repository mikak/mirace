#ifndef TRANSPORT_H
#define TRANSPORT_H

#include <QObject>
#include <QSerialPort>

#include "ChecksumVerifier.h"

class ControlEngine;

class Transport : public QObject
{
    Q_OBJECT
public:
    explicit Transport(QObject *parent = 0);
    ~Transport();

    bool init();
    bool sendPacket(const QByteArray &packet);

    void setEngine(ControlEngine *engine);

signals:

public slots:
    void readPacket();

private:
    QSerialPort mPort;
    ChecksumVerifier mVerifier;

    ControlEngine *mEngine;
};

#endif // TRANSPORT_H
