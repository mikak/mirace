#ifndef RACESTANDINGMODEL_H
#define RACESTANDINGMODEL_H

#include <QAbstractListModel>

#include "RaceTimer.h"

class RaceStandingModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum StandingRole {
        DriverRole = Qt::UserRole + 1,
        CarRole,
        ColorRole,
        LapsRole,
        LastLapTimeRole
    };

    RaceStandingModel();
    virtual ~RaceStandingModel();

    virtual int rowCount(const QModelIndex &parent) const;

    //virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    virtual QVariant data(const QModelIndex &index, int role) const;

    QHash<int, QByteArray> roleNames() const;


public slots:
    void registerLap(int id, int totalTime);

private:
    RaceTimer mRaceTimer;

    QStringList mColors;
};

#endif // RACESTANDINGMODEL_H
