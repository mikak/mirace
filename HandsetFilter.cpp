#include "HandsetFilter.h"



HandsetFilterLinear::HandsetFilterLinear(int maxOut, int minIn, int maxIn)
    : mMaxOut(maxOut), mMinIn(minIn), mMaxIn(maxIn)
{
    if (mMaxIn <= mMinIn) {
        mMaxOut = -1;
    }
}

bool HandsetFilterLinear::filterData(HandsetData &hsData)
{
    if (mMaxOut < 0) {
        return false;
    } else if (mMaxOut == 0) {
        hsData.power = 0;
        return true;
    }

    int x = hsData.power;

    if (x < mMinIn)
        x = mMinIn;
    else if (x > mMaxIn)
        x = mMaxIn;

    x -= mMinIn;
    x = x * mMaxOut / (mMaxIn - mMinIn);

    hsData.power = x;
    return true;
}
