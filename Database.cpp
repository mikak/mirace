#include "Database.h"
#include <QDebug>

Database::Database()
{
    qDebug() << mSettings.fileName();
    readDrivers();
    writeDrivers();
}

void Database::readDrivers()
{
    mDriverDb.readDrivers(mSettings);
}

void Database::writeDrivers()
{
    mDriverDb.writeDrivers(mSettings);
}

DriverDatabase &Database::driverDb()
{
    return mDriverDb;
}

