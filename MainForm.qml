import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

Item {
    id: item1
    width: 640
    height: 480

    ListView {
        id: listView1
        anchors.fill: parent
        interactive: true
        model: raceStandings

        delegate: Item {
            id: item2
            x: 5
            width: parent.width
            height: 40
            Row {
                id: row1
                anchors.verticalCenter: parent.verticalCenter
                spacing: 10

                Text {
                    id: text1
                    text: index + 1
                    anchors.verticalCenter: parent.verticalCenter
                }

                Rectangle {
                    width: 40
                    height: 40
                    color: colorCode
                }

                Text {
                    text: driver
                    anchors.verticalCenter: parent.verticalCenter
                    font.bold: true
                    width: 200
                }

                Text {
                    text: laps
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignRight
                    font.bold: true
                    font.pixelSize: 20
                    width: 60
                }

                Text {
                    text: lastLapTime
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignRight
                    font.bold: false
                    font.pixelSize: 20
                    width: 100
                }

            }
        }
    }
}
