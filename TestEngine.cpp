#include "TestEngine.h"
#include <QDateTime>

TestEngine::TestEngine(QObject *parent)
    : QObject(parent),
      mTimeElapsed(1234), mCarId(1)
{
    connect(&mTimer, SIGNAL(timeout()), SLOT(emitLap()));
    mTimer.start(500);
}

TestEngine::~TestEngine()
{
}

void TestEngine::emitLap()
{
    emit lapTimed(mCarId, mTimeElapsed);
    if (++mCarId > 6)
        mCarId = 1;
    mTimeElapsed += 500 + (qrand() % 100);
}

