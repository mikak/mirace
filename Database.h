#ifndef DATABASE_H
#define DATABASE_H

#include <QSettings>
#include <QList>

#include "DriverDatabase.h"

class Database
{
public:
    Database();

    DriverDatabase &driverDb();

private:
    void readDrivers();
    void writeDrivers();

    QSettings mSettings;

    DriverDatabase mDriverDb;
};

#endif // DATABASE_H
