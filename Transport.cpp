#include "Transport.h"
#include "PacketData.h"
#include "ControlEngine.h"

#include <QSemaphore>

#include "ChecksumVerifier.h"
#include <QDebug>

Transport::Transport(QObject *parent)
    : QObject(parent),
      mEngine(0)
{
    mPort.setPortName("/dev/ttyUSB0");
    mPort.setBaudRate(QSerialPort::Baud19200);
    mPort.setStopBits(QSerialPort::OneStop);
    mPort.setDataBits(QSerialPort::Data8);
    mPort.setParity(QSerialPort::NoParity);


    //connect(&mPort, SIGNAL())

    qDebug() << sizeof(StatusPacketRaw);
}

Transport::~Transport()
{
}

bool Transport::init()
{
    QByteArray initPacket(9, '\xFF');

    if (mPort.open(QIODevice::ReadWrite)) {

        mPort.setBaudRate(QSerialPort::Baud19200);
        mPort.setStopBits(QSerialPort::OneStop);
        mPort.setDataBits(QSerialPort::Data8);
        mPort.setParity(QSerialPort::NoParity);

       // mPort.setBaudRate(0);
       // mPort.setBaudRate(19200);

        mPort.setDataTerminalReady(true);
        mPort.setRequestToSend(true);

        connect(&mPort, SIGNAL(readyRead()), SLOT(readPacket()));
        qDebug() << mPort.pinoutSignals();

        //qDebug() << "Sync" << mPort.readAll().length();

        sendPacket(initPacket);
        //sendPacket(initPacket);
        mPort.flush();
        mPort.waitForBytesWritten(20);
        mPort.setRequestToSend(false);
        //QSemaphore s(1);
        //s.
        mPort.waitForReadyRead(1000);
        readPacket();
        return true;
    } else {
        qDebug() << "Opening port failed";
    }
    return false;
}

bool Transport::sendPacket(const QByteArray &packet)
{
    if (packet.size() != sizeof(ControlPacketRaw)) {
        qWarning() << "Invalid packet to send" << packet.size() << sizeof(ControlPacketRaw);
        return false;
    }

    mPort.setRequestToSend(true);

    QByteArray newPacket(packet);
    newPacket.chop(1);
    char checksum = mVerifier.calculateChecksum(newPacket.constData(),
                                                newPacket.size());
    //qDebug() << "Checksum" << checksum;
    newPacket.append(checksum);

    //qDebug() << "Sending" << newPacket.size() << newPacket.toHex();

    bool r = (mPort.write(newPacket) == newPacket.size());

    mPort.setRequestToSend(false);
    return r;
}

void Transport::setEngine(ControlEngine *engine)
{
    mEngine = engine;
}

void Transport::readPacket()
{
    static const int packetSize = sizeof(StatusPacketRaw);

    //char buffer[packetSize];
    //qDebug() << "readPacket" << mPort.bytesAvailable() << packetSize;

    while (mPort.bytesAvailable() >= packetSize) {
        QByteArray ba = mPort.readAll();
        //qDebug() << "Got" << ba.toHex();
        ba = ba.right(packetSize);
                //mPort.read(buffer, packetSize);
        const char *buffer = ba.constData();
        const StatusPacketRaw *packet =
                reinterpret_cast<const StatusPacketRaw *>(buffer);

        if (!mVerifier.verifyChecksum(packet)) {
            qDebug() << "Bad checksum";
            QByteArray initPacket(9, '\xFF');
            initPacket[0] = 0x7F;
            sendPacket(initPacket);
        } else if (mEngine) {
            mEngine->processPacket(packet);
        }
    }
}
