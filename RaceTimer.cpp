#include "RaceTimer.h"
#include <QDebug>

RaceTimer::RaceTimer(QObject *parent)
    : QObject(parent), mRaceStartTime(-1)
{
}

RaceTimer::~RaceTimer()
{
}

const RaceTimeData RaceTimer::timeData(int id) const
{
    return mRaceData.value(id);
}

void RaceTimer::registerLap(int id, int totalTime)
{
    if (mRaceStartTime < 0) {
        mRaceStartTime = totalTime;
    }

    int n = mRaceData[id].addLapTime(totalTime - mRaceStartTime);

    qDebug() << "** Car" << id << "laps" << n
             << "last" << mRaceData[id].lastLapTime();
}

int RaceTimeData::lastLapTime() const
{
    if (mLapTimes.count() > 1) {
        return mLapTimes.last() - mLapTimes.at(mLapTimes.count() - 2);
    } else {
        return -1;
    }
}

int RaceTimeData::addLapTime(int totalTime)
{
    mLapTimes.append(totalTime);
    return lapCount();
}
